package cn.tedu.sp12;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class Sp12ZuulApplication {

	public static void main(String[] args) {
		SpringApplication.run(Sp12ZuulApplication.class, args);
	}

}
