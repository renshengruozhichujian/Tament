package cn.tedu.m2_work;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class Producer {
    public static void main(String[] args) throws Exception {

        ConnectionFactory f= new ConnectionFactory();
        f.setHost("192.168.135.129");//192.168.135.129
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
       // f.setVirtualHost("/dg");
        Channel c = f.newConnection().createChannel();

        c.queueDeclare("helloword",true,false,false,null);

        while (true){
            System.out.println("请输入消息:");
            String msg = new Scanner(System.in).nextLine();
                if("exit".equals(msg)){
                    break;
                }
            c.basicPublish("","helloword", MessageProperties.TEXT_PLAIN,msg.getBytes());
            System.out.println("消息已发送:"+msg);
        }
        c.close();
    }
}
