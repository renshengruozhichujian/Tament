package cn.tedu.m2_work;

import com.rabbitmq.client.*;

import java.io.IOException;

public class Consumer {
    public static void main(String[] args) throws Exception{
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.135.129");//192.168.135.129
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");

        Channel c = f.newConnection().createChannel();
        c.queueDeclare("helloword",true,false,false,null);
        System.out.println("接受信息:");
        DeliverCallback callback = new DeliverCallback() {

            @Override
            public void handle(String s, Delivery delivery) throws IOException {

                String msg = new String(delivery.getBody());
                System.out.println(msg);
                for(int i=0 ; i<msg.length();i++){
                    if(msg.charAt(i)=='.'){
                        try{
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }
                }
                c.basicAck(delivery.getEnvelope().getDeliveryTag(),false);
                System.out.println("处理结束");
            }
        };
        CancelCallback cancel = new CancelCallback() {
            @Override
            public void handle(String s) throws IOException {

            }
        };
        c.basicConsume("helloword",false,callback,cancel);


    }
}
