package cn.tedu.m1.simple;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;



public class Producer {

    public static void main (String[]args) throws Exception {
        // 连接rabbitmq   服务器
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.135.129");
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");

        Connection con = f.newConnection();
        Channel c = con.createChannel();
        //定义队列
        c.queueDeclare("hellword",false,false,false,null);

        //发送消息
        /**
         * basicPublich 参数含义
         * 1.exchange
         * 2,routingKey
         * 3.props      其他参数属性
         * 4            消息
         */
        c.basicPublish("","hellword",null,"hellword".getBytes());
        System.out.println("消息已发送");
        c.close();




    }
}
