package cn.tedu.m1.simple;

import com.rabbitmq.client.*;

import java.io.IOException;

public class Consumer {


    /**
     *quequeDeclare参数含义
     * 1.队列名                         //queue
     * 2.是否是持久队列                  //durable
     * 3.是否是排他队列                 //exclusive
     * 4.如果没有消费者是否自动删除       //autodelete
     * 5.其他参数属性                  //arguments
     *
     */
        public static void main (String[]args) throws Exception{
            ConnectionFactory f = new ConnectionFactory();
            f.setHost("192.168.135.129");
            f.setPort(5672);
            f.setUsername("admin");
            f.setPassword("admin");

            Channel c = f.newConnection().createChannel();
            c.queueDeclare("helloword",false,false,false,null);

            DeliverCallback deliverCallback = new DeliverCallback() {

                @Override
                public void handle(String s, Delivery delivery) throws IOException {
                    byte[] body = delivery.getBody();
                    String msg = new String(body);
                    System.out.println("收到:"+msg);
                }
            };

            CancelCallback cancelCallback = new CancelCallback() {
                @Override
                public void handle(String s) throws IOException {

                }
            };


            c.basicConsume("hellword",true,deliverCallback,cancelCallback);

        }
}
