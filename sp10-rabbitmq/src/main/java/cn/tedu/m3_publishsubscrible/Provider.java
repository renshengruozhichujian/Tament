package cn.tedu.m3_publishsubscrible;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;

import java.util.Scanner;

public class Provider {
    public static void main(String[] args) throws Exception{
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("212.64.72.198");
        //f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");

        Channel con = f.newConnection().createChannel();
        con.exchangeDeclare("logs", BuiltinExchangeType.FANOUT);
        while (true){
            System.out.println("请输入消息");
            String msg  = new Scanner(System.in).nextLine();
            con.basicPublish("logs","",null,msg.getBytes());
            System.out.println("消息已发送"+msg);
        }

    }

}
