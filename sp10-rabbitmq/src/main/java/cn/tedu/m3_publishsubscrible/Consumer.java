package cn.tedu.m3_publishsubscrible;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.UUID;

public class Consumer {

    public static void main(String[] args) throws Exception{
        ConnectionFactory f =new ConnectionFactory();
        f.setHost("212.64.72.198");
        f.setUsername("admin");
        f.setPassword("admin");

        Channel con = f.newConnection().createChannel();
        con.exchangeDeclare("logs","fanout");
        String queue = UUID.randomUUID().toString();
        con.queueDeclare(queue,false,true,true,null);
        con.queueBind(queue,"logs","");
        DeliverCallback deliverCallback = new DeliverCallback() {
            @Override
            public void handle(String consumerTag, Delivery message) throws IOException {
                String msg = new String(message.getBody());
                System.out.println("收到消息："+msg);
            }
        };
        CancelCallback cancelCallback = new CancelCallback() {
            @Override
            public void handle(String consumerTag) throws IOException {

            }
        };
        con.basicConsume(queue,true,deliverCallback,cancelCallback);

    }
}
