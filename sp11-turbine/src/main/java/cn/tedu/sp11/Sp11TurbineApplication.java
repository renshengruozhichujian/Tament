package cn.tedu.sp11;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.turbine.EnableTurbine;

@SpringBootApplication
@EnableTurbine
public class Sp11TurbineApplication {

    public static void main(String[] args) {
        SpringApplication.run(Sp11TurbineApplication.class, args);
    }

}
